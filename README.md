## setup

### Install pimcore

`bin/install`

You can then install the packages with

`bin/install-packages`

if you want to adjust the version you can do this in the packages script 
